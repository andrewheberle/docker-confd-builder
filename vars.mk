# Image config
CONFD_VERSION = 0.16.0
BASE_IMAGE = golang
BASE_TAG = 1.12-alpine3.10

# Registry path for tagging image
REGISTRY = registry.gitlab.com/andrewheberle

# Image information/metadata
IMAGE_ARCH = amd64
IMAGE_AUTHOR = Andrew Heberle
IMAGE_DESCRIPTION = confd builder container
IMAGE_NAME = docker-confd-builder
IMAGE_VCS_BASE_URL = gitlab.com/andrewheberle

# Values to replace in the Dockerfile
DOCKERFILE_REPLACE_VARS = CONFD_VERSION BASE_IMAGE BASE_TAG IMAGE_ARCH IMAGE_AUTHOR IMAGE_DESCRIPTION IMAGE_NAME IMAGE_VCS_BASE_URL
